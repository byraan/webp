#!/usr/bin/env node
process.setMaxListeners(0);
process.emitWarning = (warning, ...args) => {
	if (args[0] === 'ExperimentalWarning') return;
	if (args[0] && typeof args[0] === 'object' && args[0].type === 'ExperimentalWarning') return;
	// return emitWarning(warning, ...args);
};
const fs=require('fs'),
path=require('path');
const commandLineArgs=require(`${__dirname}/../lib/command-line-args`),
log=require(`${__dirname}/../lib/log`);
commandLineArgs(this, false, 1);
if(!this.subcommands.length) return log('{{error}} subcommand required');
var subcommand=this.subcommands[0];
var src=path.resolve(`${__dirname}/../build/${subcommand}.js`);
if(fs.existsSync(src)) require(src);
else{
	let altSrc=`${__dirname}/../build/.cli.js`;
	if(fs.existsSync(altSrc)){
		let alt=require(altSrc);
		if(typeof alt=='function') alt(subcommand);
		else log('{{error}} can´t process request');
	}
	else log(`{{error}} subcommand ´${subcommand}´ not defined`);
}