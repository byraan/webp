process.variables={};
/**
 * HOW TO USE:
 * 
 * 
 * 
require_var("key")	// get the value to "key"
require_var("key_1", "key_2", "key_3", null)	// get the value to each key
require_var([
		"key_1",
		"key_2",
		"key_3"
])	// get the value to each key



require_var("key", "value")	// set "key" value to "value"
require_var("key_1", "key_2", "key_3", "value")	// set each key value to "value"
require_var({
		key_1: "value_1",
		key_2: "value_2",
		key_3: "value_3"
})	// set each key value to each value
require_var([
		[
			"key_1",
			"value_1"
		],
		[
			"key_2",
			"value_2"
		],
		[
			"key_3_1"
			"key_3_2"
			"key_3_3"
			"value_3"
		]
])	// set each key value to each value

// TO SAVE (EACH) KEY VALUE ONLY IF IS NOT STORED, require_var()(arguments)
/**
 */

set_variable: {
	let optional_set=false;
	process.variable=function(){
		if(arguments.length==0){
			optional_set=true;
			return process.variable;
		}
		var _optional_set=false;
		if(optional_set){
			_optional_set=true;
			optional_set=false;
		}
		var keys=[],
		values={},
		key,
		value;
		for(let i=0; i<arguments.length; i++){
			keys.push(arguments[i]);
		}
		if(keys.length>1) value=keys.pop();
		else if(keys.length==1){
			value=null;
		}
		for(let i=0; i<keys.length; i++){
			key=keys[i];
			if(typeof key=='object'){
				if(key.constructor.name=='Array'){
					let _key=([]).concat(key);
					let k;
					key={};
					for(let ki=0; ki<_key.length; ki++){
						k=_key[ki];
						let v;
						if(_optional_set) optional_set=true;
						if(typeof k=='object'){
							v=process.variable.apply(undefined, k);
							Object.keys(v).forEach(function(k2, k2i){
								values[k2]=v[k2];
							});
							continue;
						}
						v=process.variable(k);
						values[k]=v[k];
					}
					continue;
				}
				Object.keys(key).forEach(function(k, ki){
					let v=key[k];
					if(_optional_set) optional_set=true;
					v=process.variable(k, v);
					values[k]=v[k];
				});
				continue;
			}
			if(value===null || (_optional_set===true && typeof process.variables[key]!='undefined')){
				values[key]=process.variables[key];
				continue;
			}
			process.variables[key]=value;
			values[key]=process.variables[key];
		}
		return values;
	}
}

/**
 * EXAMPLE:
 * 
 * 
 * 
function example(){
	var a=variable('a', 1).a;
	var b=variable('b', 2).b;
	var c=variable('c', 3).c;
	console.log(a, b, c);
	var {a, b, c}=variable("a", "b", "c", 456);
	console.log(a, b, c);
	var {a, b, c}=variable({
		a: 7,
		b: 8,
		c: 9
	});
	console.log(a, b, c);
	var {a, b, c, d, e, f}=variable()([
		["a", 10],
		["b", "d", 11],
		["c", "e", "f", 12]
	]);
	console.log(a, b, c, d, e, f);
	// 1
	a=b=c=undefined;
	console.log(a, b, c);
	var a=variable("a").a;
	var b=variable("b").b;
	var c=variable("c").c;
	console.log('<1>', a, b, c);
	// 2
	a=b=c=undefined;
	console.log(a, b, c);
	var {a, b, c}=variable("a", "b", "c", null);
	console.log('<2>', a, b, c);
	// 3
	a=b=c=undefined;
	console.log(a, b, c);
	var {a, b, c}=variable([
		"a",
		"b",
		"c"
	]);
	console.log('<3>', a, b, c);
}

/**
 */