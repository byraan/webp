// file
const fs=require('fs'),
path=require('path'),
find=require('find');
// custom global
const commandLineArgs=require('../lib/command-line-args'),
log=require('../lib/log'),
regexp=require('../lib/regexp'),
exec=require('../lib/exec');
cli: {
	commandLineArgs(this, [
		{
			name: 'input',
			alias: 'i',
			type: String,
			defaultValue: ['.'],
			multiple: true
		}
	]);
}
for(let i=0; i<process.arguments.input.length; i++){
	// input
	let input=path.resolve(process.arguments.input[i]);
	let filePath=path.parse(input);
	// output
	let outputPath=path.dirname(input);
	let output=`${outputPath}/${filePath.name}`;
	// base64
	let base64=fs.readFileSync(input, 'utf-8');
	let parts=base64.split('base64,');
	// data
	let data=parts[1];
	// mime
	let attrs=parts[0].split(';');
	let mime=attrs[0].split(':');
	let mimetype=mime[1];
	// buffer & file
	let buffer=Buffer.from(data, 'base64');
	fs.writeFileSync(output, buffer);
}