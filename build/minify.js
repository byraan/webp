const { minify } = require('uglify-es');
const commandLineArgs=require('../lib/command-line-args'),
//
fs=require('fs'),
path=require('path'),
find=require('find'),
//
CleanCSS=require('clean-css'),
UglifyJS=require('uglify-es'),
//
esbuild=require('esbuild'),

log=require('../lib/log'),
Request=require('../lib/request');
var request=new Request();
commandLineArgs({}, [
	{
		name: 'input',
		alias: 'i',
		type: String,
		defaultValue: ['.'],
		multiple: true
	},
	{
		name: 'output',
		alias: 'o',
		type: String
	},
	{
		name: 'enclose',
		type: Boolean
	}
], 0);
var content='',
contentJson={},
min={
	'.css': function(c){
		let code={};
		code['file']={styles: c};
		return new CleanCSS().minify(code).styles;
	},
	'.js': function(c){
//#- esbuild
		return esbuild.transformSync((process.arguments.enclose?'(function(){':'')+c+(process.arguments.enclose?'}())':''), {
			minify: true
		}).code;
//#- uglifyjs
		// return UglifyJS.minify(c).code;
	},
	'.json': function(c){
		contentJson=Object.assign({}, contentJson, JSON.parse(c));
		content=contentJson;
	}
}
var ext=path.extname(process.arguments.input[0]);
if(['.js', '.css'].indexOf(ext)<0) return;
for(let i=0; i<process.arguments.input.length; i++){
	let ex=path.extname(process.arguments.input[i]);
	if(ex!=ext) continue;
	process.arguments.input[i]=path.resolve(process.arguments.input[i]);
	content+=fs.readFileSync(process.arguments.input[i], 'utf-8');
}
if(process.arguments.input.length>1 && !process.arguments.output) return log('{{error}} Define output filename');
process.arguments.output=path.resolve(process.arguments.output?process.arguments.output+(process.arguments.input.length>1?'':'/'+path.basename(process.arguments.input[0], ext)+'.min'+ext):path.dirname(process.arguments.input[0])+'/'+path.basename(process.arguments.input[0], ext)+'.min'+ext);
content=min[ext](content);
fs.writeFileSync(process.arguments.output, content);