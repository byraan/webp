const fs = require('fs'),
path = require('path');
const server = require('server');
const { get, post } = server.router,
{ render, redirect } = server.reply;

server({
	port: 8080,
	engine: 'pug',
	views: path.join(__dirname, '/gallery')
}, [
	get("/favicon.ico", (ctx) => {}),
	get("/", (ctx) => {
		var fileMap=new Map();
		if(!ctx.query.dir) ctx.query.dir="";
		var dir=process.env.npm_config_dir;
		if(!dir) dir=path.resolve(".");
		dir=`${dir}${ctx.query.dir?ctx.query.dir:''}`;
		var files=fs.readdirSync(dir);
		for(let i=0; i<files.length; i++){
			if(files[i].charAt(0)=='.') continue;
			fileMap.set(files[i], fs.statSync(`${dir}/${files[i]}`));
		}
		return render("index", {
			env: {
				path: path
			},
			dir: dir,
			query: ctx.query,
			list: Array.from(fileMap.keys()).sort(),
			file: fileMap,
			readImg: function(file){
				var bitmap=fs.readFileSync(file);
				return new Buffer.from(bitmap).toString('base64');
			}
		});
	})//,
	// get("/gallery/:id", (ctx) => render("gallery", { image: ctx.params.id })),
	/* post('/', ctx => {
		console.log(ctx.data);
		return 'ok';
	}) */
]);